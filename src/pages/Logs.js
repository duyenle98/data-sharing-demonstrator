import React from "react";
import "../assets/css/logs.scss";

const Item = ({ date, download, content, email }) => {
  return (
    <div className="item">
      {date}: {download && <span className="download">Download result</span>} {content} with {email}
    </div>
  )
}

const Logs = () => {
  return (
    <div className="page page_logs">
      <div className="wrapper">
        <div className="title">Logs</div>
        <Item
          date="27/01/21"
          download
          content="calculated"
          email="user1@gmail.com"
        />
        <Item
          date="20/01/21"
          content="calculated c-intersection"
          email="user2@gmail.com"
        />
        <Item
          date="18/01/21"
          content="calculated s+-variance"
          email="user3@gmail.com"
        />
        <div className="more">
          <i className="fa fa-ellipsis-v" />
        </div>
        <div className="note">*Note: results are automatically deleted from server after 1 day</div>
      </div>
    </div>
  )
}

export default Logs;