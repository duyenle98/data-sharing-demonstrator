import React, { useState, useRef } from "react";
import logo from "../assets/images/logo.png";
import "../assets/css/intersect.scss";
import ModalCalculation from "../components/modalCalculation";
import ModalError from "../components/modalError";
import FileParsed from "../components/fileParsed";
import ModalWaiting from "../components/modalWaiting";
import ModalSuccess from "../components/modalSuccess";

const Intersect = () => {
  const [modalCalculation, setModalCalculation] = useState(false);
  const [isSuccessCalc, setIsSuccessCalc] = useState(false);
  const [file, setFile] = useState({});
  const [modalFileError, setModalFileError] = useState(false);
  const [modalFileParsed, setModalFileParsed] = useState(false);
  const [isSuccessUpload, setIsSuccessUpload] = useState(false);
  const [email, setEmail] = useState({ name: "", isValid: false, error: "" });
  const [modalWaiting, setModalWaiting] = useState(false);
  const [isSuccessAll, setIsSuccessAll] = useState(false);
  const [timer, setTimer] = useState(null);
  const inputFile = useRef(null);

  const toggle = () => setModalCalculation(!modalCalculation);

  const handleSubmitModal = () => setIsSuccessCalc(true);

  const handleClickFile = (e) => {
    e.target.value = "";
  }
  const handleChangeFile = (e) => {
    const filePath = e.target.files[0].name;
    const allowedExtensions = /(\.csv|\.txt|\.xlsx)$/i;
    if (!allowedExtensions.exec(filePath)) {
      setFile({
        path: filePath,
        isValid: false,
      });
      setModalFileError(true);
    } else {
      setFile({
        path: filePath,
        isValid: true,
      });
      setModalFileParsed(true);
    }
  };

  const handleCloseFileErrorModal = () => setModalFileError(false);

  const toggleFileParsedModal = () => setModalFileParsed(!modalFileParsed);

  const handleSubmitFileModal = () => setIsSuccessUpload(true);

  const handleChangeEmail = (e) => {
    setEmail({
      ...email,
      name: e.target.value,
    });
  };

  const handleSubmitEmail = () => {
    const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (pattern.test(email.name)) {
      setEmail({
        ...email,
        isValid: true,
        error: ""
      });
    } else {
      setEmail({
        ...email,
        isValid: false,
        error: "Invalid email"
      });
    }
  };

  const toggleWaitingModal = () => {
    clearTimeout(timer);
    setModalWaiting(!modalWaiting);
  }

  const handleClickGoButton = () => {
    setModalWaiting(true);
    const waitingTimer = setTimeout(() => {
      setIsSuccessAll(true);
      setModalWaiting(false);
    }, 3000);
    setTimer(waitingTimer);
  };

  return (
    <div className="page page_intersect">
      <div className="circle_select">
        <div className="decoration">
          <div className="logo">
            <img src={logo} alt="" />
            <h3>Logotext</h3>
            <h6>Slogan</h6>
          </div>
          {isSuccessCalc && (
            <div className="upload">
              {isSuccessUpload ? (
                <>
                  <i className="fa fa-lock" />
                  <span>{file.path}</span>
                </>
              ) : (
                  "Upload and encrypt your data"
                )}
            </div>
          )}
          {!isSuccessUpload ? (
            <div className="icon">
              <label htmlFor="upload-file">
                <i
                  className={`fa fa-plus-circle ${isSuccessCalc ? "icon_enable" : "icon_disable"
                    }`}
                />
              </label>
              {isSuccessCalc && (
                <input
                  type="file"
                  id="upload-file"
                  ref={inputFile}
                  onChange={handleChangeFile}
                  onClick={handleClickFile}
                />
              )}
            </div>
          ) : (
              <div className="icon icon_success">
                <i className="fa fa-check-circle" />
              </div>
            )}
        </div>
        <div className="select">
          {!isSuccessCalc ? (
            <div className="content">
              <div className="title">
                Select
                    <br />
                    calculation
                  </div>
              <div className="icon" onClick={toggle}>
                <i className="fa fa-plus-circle" />
              </div>
            </div>
          ) : (
              <div className="content">
                <div className="title">
                  Intersection
                    <br />
                    Function:
                  </div>
                <div className="sub_title">C-Variance</div>
                <div className="icon icon_success">
                  <i className="fa fa-check-circle" />
                </div>
              </div>
            )}
        </div>
      </div>
      <div className="circle_party">
        <div className="wrapper">
          {isSuccessUpload && email.isValid && (
            <div className="email show">{email.name}</div>
          )}
          {isSuccessUpload && !email.isValid && (
            <>
              <div className="title">Select 2nd party</div>
              <input
                type="text"
                className="email"
                placeholder="2nd party's email..."
                value={email.name}
                onChange={handleChangeEmail}
              />
              <div className="error">{email.error}</div>
            </>
          )}
          {!email.isValid ? (
            <div className="icon">
              <i
                className={`fa fa-plus-circle ${isSuccessUpload ? "icon_enable" : "icon_disable"
                  }`}
                onClick={handleSubmitEmail}
              />
            </div>
          ) : (
              <div className="icon icon_success">
                <i className="fa fa-check-circle" />
              </div>
            )}
        </div>
      </div>
      <button
        className={`btn btn_go ${!email.isValid && "disable"}`}
        disabled={!email.isValid}
        onClick={handleClickGoButton}
      >
        Go
          </button>

      <ModalCalculation
        modal={modalCalculation}
        toggle={toggle}
        handleSubmit={handleSubmitModal}
        className="modal_calculation"
      />
      <ModalError
        modal={modalFileError}
        handleClose={handleCloseFileErrorModal}
        handleTryAgain={() => {
          inputFile.current.click();
          setModalFileError(false);
        }}
        title="File error"
        content="There were not enough columns available for the PSI computation you have selected (minimum 2 required)"
        className="modal_file_error"
      />
      <FileParsed
        modal={modalFileParsed}
        toggle={toggleFileParsedModal}
        handleSubmit={handleSubmitFileModal}
        className="modal_file_parsed"
      />
      <ModalWaiting
        modal={modalWaiting}
        className="modal_waiting"
        email={email.name}
        toggle={toggleWaitingModal}
      />
      <ModalSuccess
        modal={isSuccessAll}
        className="modal_success"
        toggle={() => setIsSuccessAll(!isSuccessAll)}
        intersection="intersection"
        totalEls="totalEls"
        size="size"
        time="time"
      />
    </div>
  );
};

export default Intersect;
