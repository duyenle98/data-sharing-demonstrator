import React from "react";
import { useHistory } from "react-router-dom";
import logo from "../assets/images/logo.png";
import "../assets/css/home.scss";

const Home = () => {
  const history = useHistory();

  return (
    <div className="page page_home">
      <div className="circle">
        <img src={logo} alt="" />
        <h3>Logotext</h3>
        <h6>Slogan</h6>
        <div>Our Demonstrator</div>
      </div>
      <button className="btn btn_login" onClick={() => {
        history.push("/intersect");
        localStorage.setItem("isLogin", true);
      }}>
        Login Now
      </button>
    </div>
  );
};

export default Home;
