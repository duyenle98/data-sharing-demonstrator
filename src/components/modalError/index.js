import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import "./modalError.scss";

const ModalError = ({
  modal,
  handleClose,
  handleTryAgain,
  title,
  content,
  className,
}) => {
  return (
    <div>
      <Modal isOpen={modal} className={className}>
        <ModalHeader>
          <i className="fa fa-times-circle" />
          <div className="title">{title}</div>
        </ModalHeader>
        <ModalBody>
          <div>{content}</div>
        </ModalBody>
        <ModalFooter>
          <Button outline color="secondary" onClick={handleTryAgain}>
            Try again
          </Button>
          <Button outline color="secondary" onClick={handleClose}>
            Close
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default ModalError;
