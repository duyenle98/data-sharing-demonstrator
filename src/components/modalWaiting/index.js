import React from "react";
import { Button, Modal, ModalBody, ModalFooter } from "reactstrap";
import "./modalWaiting.scss";

const ModalWaiting = ({ modal, toggle, email, className }) => {
  return (
    <div>
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalBody>
          <div>
            Waiting for <span>{email}</span>
            <br />
            to finish this calculation...
          </div>
        </ModalBody>
        <ModalFooter>
          <Button outline color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default ModalWaiting;
