import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Radio from "../radio";
import "./modalCalculation.scss";
import _ from "lodash";

const ModalCalculation = ({ modal, toggle, handleSubmit, className }) => {
  const [isNext, setIsNext] = useState(false);
  const [roles, setRoles] = useState([
    { title: "Client", checked: true },
    { title: "Server", checked: false },
    { title: "Server+", checked: false }
  ]);
  const [calculations, setCalculations] = useState([
    { title: "Intersection", checked: false },
    { title: "Cardinality", checked: false },
    { title: "Sum", checked: false },
    { title: "Mean", checked: false },
    { title: "Variance", checked: true },
    { title: "Range", checked: false }
  ]);

  const transformArray = (arr, index) => {
    const results = arr.map((item, i) => {
      if (i === index) {
        return {
          ...item,
          checked: true
        }
      } else {
        return {
          ...item,
          checked: false
        }
      }
    });
    return results;
  }

  return (
    <div>
      <Modal isOpen={modal} toggle={toggle} className={className}>
        {!isNext ? (
          <>
            <ModalHeader>
              <div className="icon">
                <i className="fa fa-times" onClick={toggle} />
                <i className="fa fa-chevron-left" onClick={toggle} />
              </div>
              <div className="title">Role</div>
              <div className="sub_header">
                Select which role you would like to play in this PSI protocol
                computation
              </div>
            </ModalHeader>
            <ModalBody>
              {roles.map((item, index) => (
                <Radio
                  key={index}
                  title={item.title}
                  checked={item.checked}
                  onChange={() => {
                    const newRoles = transformArray(roles, index);
                    setRoles(newRoles);
                  }}
                />
              ))}
            </ModalBody>
            <ModalFooter>
              <Button outline color="primary" onClick={() => setIsNext(true)}>
                Next
              </Button>
            </ModalFooter>
          </>
        ) : (
          <>
            <ModalHeader>
              <div className="icon">
                <i className="fa fa-times" onClick={toggle} />
                <i
                  className="fa fa-chevron-left"
                  onClick={() => setIsNext(false)}
                />
              </div>
              <div className="title">Intersection Calculation</div>
              <div className="sub_header">
                Select the calculation you would like to compute using the PSI
                protocol
              </div>
            </ModalHeader>
            <ModalBody className="py_large">
              {_.chunk(calculations, 3).map((items, index) => (
                <div key={index} className="col">
                  {items.map((item, i) => (
                    <Radio
                      key={i}
                      title={item.title}
                      checked={item.checked}
                      isInfo={false}
                      onChange={() => {
                        const newCalculations = transformArray(calculations, i + 3 * index);
                        setCalculations(newCalculations);
                      }}
                    />
                  ))}
                </div>
              ))}
            </ModalBody>
            <ModalFooter>
              <Button
                outline
                color="primary"
                onClick={() => {
                  handleSubmit();
                  toggle();
                }}
              >
                Submit
              </Button>
            </ModalFooter>
          </>
        )}
      </Modal>
    </div>
  );
};

export default ModalCalculation;
