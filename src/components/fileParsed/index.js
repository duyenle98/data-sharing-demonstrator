import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Menu from "../dropdownMenu";
import "./fileParsed.scss";

const FileParsed = ({ modal, toggle, handleSubmit, className }) => {
  return (
    <div>
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader>
          <div className="icon">
            <i className="fa fa-times" onClick={toggle} />
            <i className="fa fa-chevron-left" onClick={toggle} />
          </div>
          <div className="title">
            <i className="fa fa-check-circle" />
            File parsed
          </div>
          <div className="sub_header">
            Select the column you would like to use for the intersection and
            computation
          </div>
        </ModalHeader>
        <ModalBody>
          <Menu
            title="Intersection column"
            options={[
              "EMAILS [column 1]",
              "MONETARY_VAL [column 2]",
              "PHONE_NO [column 3]",
              "NAME [column 4]",
            ]}
          />
          <Menu
            title="Associated value column"
            options={["MONETARY_VAL [column 2]", "PHONE_NO [column 3]"]}
          />
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            onClick={() => {
              handleSubmit();
              toggle();
            }}
          >
            Next
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default FileParsed;
