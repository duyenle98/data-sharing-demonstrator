import React, { useState } from "react";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import "./dropdownMenu.scss";

const Menu = ({ title, options }) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState(title);

  const toggle = () => setDropdownOpen((pre) => !pre);

  return (
    <Dropdown
      group
      isOpen={dropdownOpen}
      size="sm"
      toggle={toggle}
      className="menu"
    >
      <DropdownToggle caret>{selectedItem}</DropdownToggle>
      <DropdownMenu onClick={(e) => setSelectedItem(e.target.textContent)}>
        {options.map((item, index) => (
          <DropdownItem key={index}>{item}</DropdownItem>
        ))}
      </DropdownMenu>
    </Dropdown>
  );
};

export default Menu;
