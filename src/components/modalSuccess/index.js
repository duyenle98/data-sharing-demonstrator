import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import "./modalSuccess.scss";

const ModalSuccess = ({
  modal,
  toggle,
  handleSubmit,
  intersection,
  totalEls,
  size,
  time,
  className,
}) => {
  return (
    <div>
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader>
          <div className="title">
            <i className="fa fa-check-circle" />
            <span>Success</span>
          </div>
        </ModalHeader>
        <ModalBody>
          <div className="title">
            <p>
              <b>Mean of intersection:</b>
            </p>
            <p>Total number of elements:</p>
            <p>Communication size:</p>
            <p>Time taken:</p>
          </div>
          <div className="content">
            <p>{intersection}</p>
            <p>{totalEls}</p>
            <p>{size}</p>
            <p>{time}</p>
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggle}>
            Download result
          </Button>
          <Button color="secondary" onClick={toggle}>
            Recompute
          </Button>
          <Button color="info" onClick={toggle}>
            Close connection
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default ModalSuccess;
