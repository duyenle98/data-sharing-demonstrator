import React from "react";
import "./radio.scss";

const Radio = ({ title, checked, isInfo = true, onChange }) => {
  return (
    <div className="radio">
      <label className="title">
        {title}
        <input type="radio" checked={checked} name="radio" onChange={onChange} />
        <span className="checkmark"></span>
      </label>
      {isInfo && <i className="fa fa-info-circle" />}
    </div>
  );
};

export default Radio;
