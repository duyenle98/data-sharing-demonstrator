import React from "react";
import Layout from "../layout";

export default function withLayout(Component) {
  return function LayoutWrapper(props) {
    return <Layout {...props} Content={Component} />;
  };
}
