import React, { Suspense } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Spinner } from "reactstrap";
import withLayout from "./hocs/withLayout";
const Home = withLayout(React.lazy(() => import("./pages/Home")));
const Guide = withLayout(React.lazy(() => import("./pages/Guide")));
const About = withLayout(React.lazy(() => import("./pages/About")));
const Intersect = withLayout(React.lazy(() => import("./pages/Intersect")));
const Logs = withLayout(React.lazy(() => import("./pages/Logs")));

function App() {
  return (
    <Suspense fallback={<Spinner color="primary" />}>
      <Router>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/guide" exact component={Guide} />
          <Route path="/about" exact component={About} />
          <Route path="/intersect" exact component={Intersect} />
          <Route path="/logs" exact component={Logs} />
        </Switch>
      </Router>
    </Suspense>
  );
}

export default App;
