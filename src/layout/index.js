import React from "react";
import Footer from "./Footer";
import Header from "./Header";

const Layout = ({ Content, ...props }) => {
  return (
    <div className="app_layout">
      <Header pageProps={props} />
      <Content {...props} />
      <Footer />
    </div>
  );
};

export default Layout;
