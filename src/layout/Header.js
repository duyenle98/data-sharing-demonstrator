import React from "react";
import { NavLink } from "react-router-dom";
import logo from "../assets/images/logo.png";
import "./Header.scss";

const Header = () => {
  return (
    <div className="header">
      <div className="brand">
        <h1>Logo</h1>
        <div className="logo">
          <img src={logo} alt="" />
          <h3>Logotext</h3>
          <h6>Slogan</h6>
        </div>
      </div>
      <div className="menu">
        <ul>
          <li><NavLink to="/guide">User Guide</NavLink></li>
          <li><NavLink to="/about">About Us</NavLink></li>
          <li><NavLink to="/intersect">Intersect</NavLink></li>
          {localStorage.getItem("isLogin") === "true" && (<li><NavLink to="/logs">Logs</NavLink></li>)}
        </ul>
      </div>
    </div>
  );
};

export default Header;
